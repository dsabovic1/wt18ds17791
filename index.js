const express = require('express');
const bodyParser = require('body-parser'); //omogućava da se tijelo zahtjeva automatski parsira
var multer  = require('multer');
const app = express(); //instanca express aplikacije
var path = require('path');
var url = require('url');
const fs = require('fs');
const db = require('./db.js');

db.sequelize.sync().then(function(){ 
  /*inicializacija().then(function(){
        console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
        process.exit();
    });*/
});

let naziv = "";
var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        naziv = req.body.naziv; 
        cb(null, 'public/pdf');
        
    },
    filename: function (req, file, cb) {
        if (!fs.existsSync('public/pdf/'+naziv + '.pdf') && file.mimetype == 'application/pdf')  
            cb(null, req.body.naziv+'.pdf');
        else cb(null, req.body.naziv);
    },
});

var upload = multer({ storage: storage}); //multer settings

app.use(bodyParser.json()); //za JSON
app.use(bodyParser.urlencoded({ extended: true })); //za x-www-form-urlencoded podatke

//Spirala 4 se nalazi ispod spirale 3



//******************************************************
//Spirala 3
//******************************************************

//------------------------------------------------------
//Zadatak 1.
//------------------------------------------------------
app.use(express.static('public')); //Da bi se prikazao i css i js
app.use(express.static('public/pdf'));
//------------------------------------------------------
//Zadatak 2.
//------------------------------------------------------
app.post('/addZadatak', upload.single('postavka'), function (req, res) { 
    validan = true;
    naziv = req.body.naziv; 
    fajl = req.file;
    postavka = "http://localhost:8080/pdf/" + naziv + '.pdf';
    if (fajl)
    {
        if (fajl.mimetype != 'application/pdf' || fs.existsSync('public/json/'+naziv + 'Zad.json')) validan = false;
        if(!naziv || naziv=="") res.sendFile(path.join(__dirname + '/public/greska.html'));
        if (!validan) {
            fs.unlinkSync(path.join(__dirname+'/public/pdf',naziv));
            res.sendFile(path.join(__dirname+'/public','greska.html'));
        }
        else {
            //Kreiramo JSON fajl
            var json = "{\"naziv\": \""+ naziv + "\",\"postavka\": \""+ postavka + "\"}";
            fs.writeFile("public/json/" + naziv + "Zad.json",json, (err) => {
                if (err) throw err;
                console.log('The file has been saved!');
                res.setHeader('Content-Type', 'application/json');
                //Ispravljeno dodantno
                //res.sendFile(path.join(__dirname+'\\public\\json\\',naziv+'Zad.json'));
                res.sendFile(path.join(__dirname+'/public/json/',naziv+'Zad.json'));
            });
            let novaLinija = naziv+","+postavka+"\n";;
            fs.appendFile('zadaci.csv', novaLinija, function(err) {
                if (err) throw err;
            }); 
            //Unos zadatka u bazu
            db.zadatak.create({naziv:naziv, postavka:postavka}).then(task=>{console.log("upisZadatka")});
        }
    }
    else res.sendFile(path.join(__dirname + '/public/greska.html'));   
});



//------------------------------------------------------
//Zadatak 3.
//------------------------------------------------------
app.get('/zadatak', function (req, res) { //url parametar naziv
    let p = url.parse(req.url, true).query;
    let fajlName = p['naziv'];
    res.setHeader('Content-Type', 'application/pdf');
    //Vracamo pdf fajl ako postoji
    if (fs.existsSync('public/pdf/'+ fajlName)) {
        res.sendFile(path.join(__dirname+'/public/pdf',fajlName));
    }
    else
        res.sendFile(path.join(__dirname+'/public','greska.html'));

});

//------------------------------------------------------
//Zadatak 4.
//------------------------------------------------------
app.post('/addGodina', function (req, res) { //url parametar naziv
    //Preuzimanje parametara
    let tijelo=req.body;
    let nazivGodine = tijelo['nazivGod'];
    db.godina.findOne({where:{naziv:nazivGodine}}).then(g => {
        if (g!=null) 
            res.sendFile(path.join(__dirname+'/public','greska.html'));
        else {
            db.godina.findOrCreate({where:{naziv:nazivGodine}, defaults: {nazivRepSpi:tijelo['nazivRepSpi'], nazivRepVje:tijelo['nazivRepVje']}}).then( console.log("Dodana godina"));
            res.redirect('addGodina.html'); //Vrsimo redirekciju
        }
    }).catch(function(err) {
        res.sendFile(path.join(__dirname+'/public','greska.html'));
    });
});
                   
//------------------------------------------------------
//Zadatak 5.
//------------------------------------------------------
app.get('/godine', function (req, res) { 
    var prvi = 1;
    db.godina.findAll().then(godine=> {
        if (godine.length==0) {json="{}"; }
        else if (godine.length==1) {
            json="{\"nazivGod\":\"" + godine[0].naziv + "\",\"nazivRepVje\":\""+ godine[0].nazivRepVje + "\",\"nazivRepSpi\":\""+ godine[0].nazivRepSpi+ "\"}";
        }
        else {
            var json="[";
            //godine - niz svih instanci godina
            for (var i=0; i<godine.length; i++) {
                nazivGod = godine[i].naziv;
                nazivRepSpi = godine[i].nazivRepSpi;
                nazivRepVje = godine[i].nazivRepVje;
                if (prvi!=1) {
                    json +=  ",{\"nazivGod\":\"" + nazivGod + "\",\"nazivRepVje\":\""+ nazivRepVje + "\",\"nazivRepSpi\":\""+ nazivRepSpi+ "\"}";
                }
                else {
                    prvi = 0;
                    json += "{\"nazivGod\":\"" + nazivGod + "\",\"nazivRepVje\":\""+ nazivRepVje + "\",\"nazivRepSpi\":\""+ nazivRepSpi+ "\"}";
                }
            }
            json += "]";
        }
        res.setHeader('Content-Type', 'application/json');
        res.end(json);
        
    });
    
});

//------------------------------------------------------
//Zadatak 7.
//------------------------------------------------------
app.get('/zadaci', function (req, res) {
    let acceptHeader = req.get('Accept');
    var prvi = 1;
    db.zadatak.findAll().then(zadaci=> {
        if (acceptHeader.includes("application/json")) {
            if (zadaci.length == 1 ) {
                json = "{\"naziv\":\"" + zadaci[0].naziv + "\",\"postavka\":\""+ zadaci[0].postavka + "\"}";
            }
            else {
                var json = "[";
                for (i in zadaci) {         
                    try {
                        if (prvi!=1) {
                            json +=",{\"naziv\":\"" + zadaci[i].naziv + "\",\"postavka\":\""+ zadaci[i].postavka + "\"}";
                        }
                        else {
                            prvi = 0;
                            json += "{\"naziv\":\"" + zadaci[i].naziv + "\",\"postavka\":\""+ zadaci[i].postavka + "\"}";
                        }
                    }
                    catch(err) {
                        console.log("error");
                    } 
                }
                json += "]";
            }
            res.setHeader('Content-Type', 'application/json');
            res.send(json);
        }
        else if (acceptHeader.includes("application/xml") || acceptHeader.includes("text/xml")) {
            let xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<zadaci>\n";
            for (i in zadaci) {
                xml+="\t<zadatak>\n";
                try {
                    xml+="\t\t<naziv> "+ zadaci[i].naziv+ " </naziv>\n";
                    xml+="\t\t<postavka> " + zadaci[i].postavka+ "</postavka>\n";
                }
                catch(err) {
                    console.log("error");
                }
                xml+="\t</zadatak>\n";
            }
            xml+= "</zadaci>"
            res.setHeader('Content-Type', 'application/xml');
            res.send(xml);
        }
        else if (acceptHeader.includes("text/csv")) {
            res.setHeader('Content-Type', 'text/csv');
            csvFajl = "";
            for (i in zadaci) {
                csvFajl += zadaci[i].naziv + "," + zadaci[i].postavka + "\n";
            }
            res.send(csvFajl);
        } 
    });
});

//******************************************************
//Spirala 4
//******************************************************

//------------------------------------------------------
//Zadatak 1.
//------------------------------------------------------
//uradjen unutar spirale 3

//------------------------------------------------------
//Zadatak 2.
//------------------------------------------------------


app.get('/vjezbeBaza', function (req, res) { 
    var prvi = 1;
    db.vjezba.findAll().then(vjezbe=> {
        if (vjezbe.length==0) {json="{}"; }
        else if (vjezbe.length==1) {
            json="{\"nazivVjezbe\":\"" + vjezbe[0].naziv+"\","+ "\"id\":\"" + vjezbe[0].id + "\"}";
        }
        else {
            var json="[";
            for (var i=0; i<vjezbe.length; i++) {
                nazivVjezbe = vjezbe[i].naziv;
                if (prvi!=1) {
                    json +=  ",{\"nazivVjezbe\":\"" + nazivVjezbe+ "\"," + "\"id\":\"" + vjezbe[i].id + "\"}";
                }
                else {
                    prvi = 0;
                    json += "{\"nazivVjezbe\":\"" + nazivVjezbe+ "\"," + "\"id\":\"" + vjezbe[i].id + "\"}";
                }
            }
            json += "]";
        }
        //console.log(json);
        res.setHeader('Content-Type', 'application/json');
        res.end(json);
        
    });
    
});

app.get('/zadaciBaza/:idVjezbe', function (req, res) { 
    //Samo oni zadaci koji nisu već dodani odabranoj vjezbi
    let idVjezbe = req.params.idVjezbe;
    var listaPovezanih = new Array();
    //console.log("idVjezbe: " + idVjezbe);
    //Lista zadataka vjezbe
    db.vjezba.findOne({where:{id:idVjezbe}}).then(function(vjezba) {
        if (vjezba) {
            vjezba.getZadaci().then(function(resSet){
                resSet.forEach(zad => {
                   // console.log(zad.id + " ");
                    listaPovezanih.push(zad.id);
                });
            }).then(function() {
                var prvi = 1;
               // console.log("Lista povezanih: \n" + listaPovezanih);
                db.zadatak.findAll({
                    where:{
                        id: {
                            [db.Op.notIn]: listaPovezanih
                        }
                    }
                }).then(zadaci=> {
                    if (zadaci.length==0) {json="{}"; }
                   
                    else if (zadaci.length==1) {
                        json="{\"id\":\"" + zadaci[0].id+"\",\"naziv\":\"" + zadaci[0].naziv+"\",\"postavka\":\"" + zadaci[0].postavka+"\"}";
                    }
                    else {
                        var json="[";
                        for (var i=0; i<zadaci.length; i++) {
                            if (prvi!=1) {
                                json +=  ",{\"id\":\"" + zadaci[i].id+"\",\"naziv\":\"" + zadaci[i].naziv+"\",\"postavka\":\"" + zadaci[i].postavka+"\"}";
                            }
                            else {
                                prvi = 0;
                                json += "{\"id\":\"" + zadaci[i].id+"\",\"naziv\":\"" + zadaci[i].naziv+"\",\"postavka\":\"" + zadaci[i].postavka+"\"}";
                            }
                        }
                        json += "]";
                    }
                    //console.log(json);
                    res.setHeader('Content-Type', 'application/json');
                    res.end(json);
                    
                });
            });
        }
        
    });
    
});


app.post('/addVjezba', function (req, res) {
    //Preuzimanje parametara
    let tijelo=req.body;
    let nazivGodine = tijelo['sGodine'];
    let idVjezbe = tijelo['sVjezbe'];
    let novaVjezba = tijelo['naziv'];
    let spirala = tijelo['spirala'];
    if (novaVjezba == null) { //2.a
      //  console.log(nazivGodine); console.log(idVjezbe);
        //pretraga idVjezbe
        db.godina.findOne({where:{naziv:nazivGodine}}).then(function(godina){
            db.vjezba.findOne({where:{id:idVjezbe}}).then(function(vjezba) {
                godina.addVjezbe(idVjezbe);
               //console.log(idVjezbe);
            })
        }).then(function(){res.redirect('/addVjezba.html')})
        .catch(function(err){res.redirect('/greska.html')});
    }
    else { //2.b
        //Kreiramo novu vjezbu
        let s = false; //Spirala
        //Provjera da li je checked spirala, ako nije undefined
        if (spirala) {
            s = true;
        }
        //console.log(s);
        db.vjezba.create({naziv:novaVjezba, spirala:s});
        //Povezujemo vjezbu i godinu
        db.godina.findOne({where:{naziv:nazivGodine}}).then(function(godina){
            db.vjezba.findOne({where:{naziv:novaVjezba}}).then(function(vjezba) {
                godina.addVjezbe(vjezba.id);
                //console.log(vjezba.id);
                //Vrsimo redirekciju nakon dodavanja
                //res.redirect('/addVjezba.html');
            })
        }).then(function(){res.redirect('/addVjezba.html')})
        .catch(function(err){res.redirect('/greska.html')});
    }
});

app.post('/vjezba/:idVjezbe/zadatak', function (req, res) {
    console.log("Pozvan post");
    let tijelo = req.body;
    let idZadatka = tijelo['sZadatak'];
    idVjezbe = req.params.idVjezbe;
    //console.log("Id: " + idZadatka);
    //console.log("Id vjezbe: " + idVjezbe);
    //Povezujemo zadatak i vjezbe
    db.zadatak.findOne({where:{id:idZadatka}}).then(function(zadatak){
        zadatak.addVjezbe(idVjezbe);
    }).then(function(){res.redirect('/addVjezba.html')})
    .catch(function(err){res.redirect('/greska.html')});
});


//------------------------------------------------------
//Zadatak 3.
//------------------------------------------------------

app.get('/godinaBaza', function(req,res) {
    god = req.body['sGodina'];
    console.log(god);
    godina = db.godina.findOne({where:{naziv:god}});
    console.log(goidna);
    res.setHeader('Content-Type', 'application/json');
    res.end(godina);
});

app.post('/studenti', function (req, res) {

});
   

app.listen(8080); //Port za osluskivanje 8080

