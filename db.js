const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2018","root","root",{host:"localhost",dialect:"mysql",logging:false});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;
db.Op = Sequelize.Op;

//import modela
db.student = sequelize.import(__dirname+'/student.js');
db.godina = sequelize.import(__dirname+'/godina.js');
db.zadatak = sequelize.import(__dirname+'/zadatak.js');
db.vjezba = sequelize.import(__dirname+'/vjezba.js');

//relacije
// Veza n-1 vise studenata moze biti na jednoj godini
// tj. 1-n na jednoj godini moze biti vise studenata
//, fk studentGod, as studenti
db.godina.hasMany(db.student, {as: 'studenti', foreignKey:"studentGod"}); //adds studentiId to db.student and studentGod to db.godina 

//godina -- više na više -- vjezba, mt godina_vjezba, fk idgodina i idvjezba, as godine i vjezbe
// Veza n-m godina moze imati vise vjezbi, a vjezba vise godina
db.godinaVjezba=db.vjezba.belongsToMany(db.godina,{as:'godine',through:'godina_vjezba',foreignKey:'idvjezba'});
db.godina.belongsToMany(db.vjezba,{as:'vjezbe',through:'godina_vjezba',foreignKey:'idgodina'});

//vjezba -- više na više -- zadatak, mt vjezba_zadatak, fk idvjezba i idzadatak, as vjezbe i zadaci 
// Veza n-m vjezba moze imati vise zadataka, a zadatak vise vjezbi
db.vjezbaZadatak=db.zadatak.belongsToMany(db.vjezba,{as:'vjezbe',through:'vjezba_zadatak',foreignKey:'idzadatak'});
db.vjezba.belongsToMany(db.zadatak,{as:'zadaci',through:'vjezba_zadatak',foreignKey:'idvjezba'});

module.exports=db;