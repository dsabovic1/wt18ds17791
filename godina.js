const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Godina = sequelize.define("godina",{
        id: {type: Sequelize.INTEGER, primaryKey:true, autoIncrement: true},
        naziv:{
            type: Sequelize.STRING, 
            unique:true
        },
        nazivRepSpi:Sequelize.STRING,
        nazivRepVje:Sequelize.STRING,
        
    }, 
    {
        timestamps: false,
        //freezeTableName: true,
       // tableName: 'godina'
    })
    return Godina;
};
