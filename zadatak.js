const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Zadatak = sequelize.define("zadatak",{
        id: {type: Sequelize.INTEGER, primaryKey:true, autoIncrement: true},
        naziv:{type: Sequelize.STRING, unique:true},
        postavka:Sequelize.STRING
    }, 
    {
        timestamps: false,
        //freezeTableName: true,
        //tableName: 'zadatak'
    })
    return Zadatak;
};
