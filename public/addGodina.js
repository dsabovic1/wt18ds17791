window.onload=function(){
	var mojDiv=document.getElementById("glavniSadrzaj");
	new GodineAjax(mojDiv); //Poziv konstruktora
}

function validiraj() {
	//Pojedincani unos
	var mojDiv=document.getElementById("porukeAddGodina");
	var validacija = new Validacija(mojDiv);
	var inputGodina=document.getElementById("nGodine");
	var inputNazivVjezbe=document.getElementById("nVjezbe");
	var inputNazivSpirale=document.getElementById("nSpirale");
	v1 = validacija.godina(inputGodina);
	var regex = /^wt\d\d\w\w\d\d\d\d\d$/
	v2 = validacija.repozitorij(inputNazivVjezbe, regex);
	v3 = validacija.repozitorij(inputNazivSpirale, regex);
	if (v1 && v2 && v3) return true;
	return false;
}
