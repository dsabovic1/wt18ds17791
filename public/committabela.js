var CommitTabela=(function(){
	//lokalne variable idu ovdje
	var tbl;
	var indexiCommitaPoRedovima = [];
	function maksimalniBrojKolona() {
		var max = 1; //Samo kolona Zadaci, nema commita
		for (var i=1; i< tbl.rows.length; i++) {
			//Prolazi kroz redove tabele i trazi maksimalan broj kolona
			var br_kol = tbl.rows[i].cells.length;
			if (br_kol > max) max = br_kol;
		}
		return max;
	}
	function validirajParametre(rbZadatka, rbCommita) {
		var brRedova = tbl.rows.length;
		if (rbZadatka<0 || rbZadatka >= brRedova) { 
			return false;
		}
		var r = tbl.rows[rbZadatka];
		var brCelija = r.cells.length;
		if (rbCommita<0 || rbCommita >= brCelija) {
			return false;
		}
		//Slucaj kada je prazan trazeni commit - nema linka
		if(rbCommita == brCelija-1) {
			if (!r.cells[r.cells.length-1].hasChildNodes()) {
				//Nema link 
				return false;
			}
		}
		return true;

	}
	function praznaTabela(max) {
		if (max>2) return false;
		for (var i=1; i<tbl.rows.length; i++) {
			if (tbl.rows[i].cells[1].hasChildNodes()) return false;
		}
		return true;
	}
	function dodajLink(celija, url, nazivCelije) {
		var link = document.createElement('a');
		link.setAttribute("href", url);
		link.appendChild(document.createTextNode(nazivCelije));
		celija.appendChild(link);
	}
	var konstruktor=function(divElement,brojZadataka){
		var kreirajTabeluString="<table id=\"tbl\"><tr><th>Naziv zadatka</th><th colspan=\"1\">Commiti</th></tr>"; //Header tabele
		for (var i=1; i<=brojZadataka; i++) {
			kreirajTabeluString+="<tr><td>Zadatak"+
			i+"</td><td colspan=\"1\"></td></tr>"; //Whitespace se racuna kao text, pa bi element imao child node ako se slucajno stavi space izmedju <td>
		}
		kreirajTabeluString+="</table>";
		//Kreiramo tabelu
		divElement.innerHTML=kreirajTabeluString;
		//Pomocni niz
		for (var i=0; i<brojZadataka; i++) {
			indexiCommitaPoRedovima.push(0); //Pocetni index - nema commita
		}
		return{
			dodajCommit:function(rbZadatka,url){
				if (rbZadatka<0 || rbZadatka >= brojZadataka) {
					console.log("Pogresni parametri");
					return -1;
				}
				rbZadatka++; //Indeksacija od nula - parametar koji se salje modulu za dodavanje commita prvog zadatka je 0, a nama zadaci pocinju od 1
				tbl = document.getElementById("tbl");
				if (tbl==null) return -1; //Greska
				var max = maksimalniBrojKolona();
				//Trazeni red
				var red = tbl.rows[rbZadatka];
				var slucaj = 1;
				//Provjera da li je tabela prazna, tj. bez commita
				if (praznaTabela(tbl, max)) {
					dodajLink(red.cells[1], url, ++indexiCommitaPoRedovima[rbZadatka-1]);
					return;
				}
				if (red.cells.length<max) slucaj=0; //U datom redu ima praznog prostora
				if (slucaj==0) {
					//Dodajemo u prazno mjesto novu celiju 
					var d = red.cells.length-1;
					var newCell = red.insertCell(d);
					dodajLink(newCell, url, ++indexiCommitaPoRedovima[rbZadatka-1]);
					//Izmjenimo colspan zadnjeg elementa
					var zadnji=red.cells[d+1];
					zadnji.setAttribute("colspan", max-red.cells.length+1);
				}
				else { //U redu imamo maksimalan broj ćelija
					//Provjera da li trebamo dodavati nove kolone ili ne
					zadnjaCelija = red.cells[red.cells.length-1];
					if (zadnjaCelija.hasChildNodes()) {
						//Prvi red povecamo colspan zadnje celije
						var prviRed = tbl.rows[0];
						prviRed.cells[prviRed.cells.length-1].setAttribute("colspan", max);
						//Dodajemo u svaki red novu celiju ili mijenjamo colspan
						for (var i=1; i< tbl.rows.length; i++) {
							trenutniRed = tbl.rows[i];
							zadnja = trenutniRed.cells[trenutniRed.cells.length-1];
							if (zadnja.hasChildNodes()) {
								//Dodajemo novu celiju
								var newCell = trenutniRed.insertCell(-1);
								newCell.setAttribute("colspan", 1);
							}
							else {
								//Mijenjamo colspan
								var zadnji = trenutniRed.cells[trenutniRed.cells.length-1];
								colspan = parseInt(zadnji.getAttribute("colspan"));
								zadnji.setAttribute("colspan", colspan+1);
							}
						}
						//Dodajmo link
						var d = red.cells.length-1;
						dodajLink(red.cells[d], url, ++indexiCommitaPoRedovima[rbZadatka-1]);
					}
					else {
						//Samo ubacujemo link
						dodajLink(zadnjaCelija, url, ++indexiCommitaPoRedovima[rbZadatka-1]);
					}
				}
				
			},
			editujCommit:function(rbZadatka,rbCommita,url){
				rbZadatka++; rbCommita++; //Indeksacija od 0, a nama u tabeli trazeni redovi i kolone od 1
				tbl = document.getElementById("tbl");
				//Metoda mijenja url commita ili vraća -1 ako su parametri pogresni
				if (tbl==null) return -1;
				if (!validirajParametre(rbZadatka, rbCommita)) {
					console.log("Pogresni parametri");
					return -1;
				}
				var red = tbl.rows[rbZadatka];
				var brCelija = red.cells.length;
				//Parametri su validni => Editujemo ih
				link = red.cells[rbCommita].childNodes;
				link[0].setAttribute("href", url);
			},
			obrisiCommit:function(rbZadatka,rbCommita){
				rbZadatka++; rbCommita++;
				if (!validirajParametre(rbZadatka, rbCommita)) {
					console.log("Pogresni parametri");
					return -1;
				}
				tbl = document.getElementById("tbl");
				//Metoda brise commit ili vraća -1 ako su parametri pogresni
				var red = tbl.rows[rbZadatka];
				var brCelija = red.cells.length;
				//Obrisimo odgovarajucu celiju
				max = maksimalniBrojKolona(tbl);
				if (brCelija==max) {
					//Prvi slucaj: Zadnja celija prazna
					if (!red.cells[red.cells.length-1].hasChildNodes()) { //Ako je zadnji prazan samo brisemo odgovarajuci el i povecavmao colspan zadnje celije
						//Brisemo odgovarajuci el i povecavamo colspan zadnjeg
						red.deleteCell(rbCommita);
						red.cells[red.cells.length-1].setAttribute("colspan", max - red.cells.length+1);
					}
					else { //Zadnja celija ima link
						//Drugi slucaj: Brisemo zadnji element u redu
						if (max==2) {
							red.cells[1].removeChild(red.cells[1].childNodes[0]); //Uklanjamo link
							return;
						}
						//Provjera da li postoji jos neki rad sa maksimalnim brojem punih celija							
						var postoji = false;
						for (var i=1; i<tbl.rows.length; i++) {
							if (i==rbZadatka) continue;
							var r = tbl.rows[i];
							if (r.cells.length == max) {
								if (r.cells[r.cells.length-1].hasChildNodes()) {
									postoji=true;
									break;
								}
							}
						}
						//Treci slucaj: Postoji jos barem jedan red sa istim brojem commita
						if (postoji) {
							//Brisemo odgovarajuci cvor i dodajemo praznu celiju na kraj
							red.deleteCell(rbCommita);
							var newCell = red.insertCell(red.cells.length);
							newCell.setAttribute("colspan", 1);
						}
						else { //Cetvrti slucaj: Ne postoji red sa vecim brojem commita
							//Brisemo element i mjenjamo colspan ili brisemo zadnju celiju u ostalim redovima
							red.deleteCell(rbCommita);
							//Mijenjamo colspan zadnje celije prvog reda
							var prviRed = tbl.rows[0];
							prviRed.cells[prviRed.cells.length-1].setAttribute("colspan", max-1);
							//Brisemo iz svakog reda zadnju celiju(ako je colspan="1") ili mijenjamo colspan
							for (var i=1; i<tbl.rows.length; i++) {
								if (i==rbZadatka) continue;
								trenutniRed = tbl.rows[i];
								var zadnji = trenutniRed.cells[trenutniRed.cells.length-1];
								colspan = parseInt(zadnji.getAttribute("colspan"));
								if (colspan>1) {
									zadnji.setAttribute("colspan", colspan-1);
								}
								else {
									//brisemo ćeliju
									trenutniRed.deleteCell(trenutniRed.cells.length-1);
								}
							}
						}
					}
				}
				else {
					//Brisemo odgovarajuci el i povecavamo colspan zadnjeg
					red.deleteCell(rbCommita);
					red.cells[red.cells.length-1].setAttribute("colspan", max - red.cells.length+1);
				}				
			}
		}
	}
	return konstruktor;
}());
