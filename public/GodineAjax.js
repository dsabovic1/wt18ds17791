//------------------------------------------------------
//Zadatak 6.
//------------------------------------------------------
var GodineAjax = (function(){
    var konstruktor = function(divSadrzaj){
        //Pravimo zahtjev
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            //Ispis godina
            if (this.readyState == 4 && this.status == 200) {
                //Kao rezultat GET zahtjeva dobili smo JSON
                json_niz = JSON.parse(this.response);
                console.log(json_niz);
                let rez = "<div class =\"folder\">\n";
                for (i in json_niz) {
                    rez += "\t<div class =\"godina\">\n";
                    rez += "\t\t<h3>"+ json_niz[i].nazivGod + "<\/h3>\n";
                    rez += "<p><em>Naziv repozitorija vjezbi: <\/em>"+ 
                    json_niz[i].nazivRepVje + "<br> <em>Naziv repozitorija spirale: <\/em>"+ 
                    json_niz[i].nazivRepSpi + "<br><\/p> <\/div>";
                }
                rez+= "<\/div>";
                divSadrzaj.innerHTML = rez;
            }
        }
        xhttp.open("GET", "http://localhost:8080/godine", true);
        xhttp.setRequestHeader('Content-Type', 'application/json');
        xhttp.send();
        return {
            osvjezi:function(){
                //Pravimo zahtjev
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    //Ispis godina
                    if (this.readyState == 4 && this.status == 200) {
                        //Kao rezultat GET zahtjeva dobili smo JSON
                        json_niz = JSON.parse(this.response);
                        console.log(json_niz);
                        let rez = "<div class =\"folder\">\n";
                        for (i in json_niz) {
                            rez += "\t<div class =\"godina\">\n";
                            rez += "\t\t<h3>"+ json_niz[i].nazivGod + "<\/h3>\n";
                            rez += "<p><em>Naziv repozitorija vjezbi: <\/em>"+ 
                            json_niz[i].nazivRepVje + "<br> <em>Naziv repozitorija spirale: <\/em>"+ 
                            json_niz[i].nazivRepSpi + "<br><\/p> <\/div>";
                        }
                        rez+= "<\/div>";
                        divSadrzaj.innerHTML = rez;
                    }
                }
                xhttp.open("GET", "/godine", true);
                xhttp.setRequestHeader('Content-Type', 'application/json');
                xhttp.send();
            }
        }
    }
    return konstruktor;
}());

