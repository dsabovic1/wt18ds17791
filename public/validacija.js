var Validacija=(function(){
	//lokalne variable idu ovdje
	var nizNevalidnih;
	function ispisi(divElementPoruke) {
		var duzinaNiza = nizNevalidnih.length;
		console.log("Ispisi pozvana");
		console.log(nizNevalidnih);
		var poruka = "Sljedeca polja nisu validna: ";
		for (var i=0; i<duzinaNiza-1; i++) {
			poruka+= nizNevalidnih[i] + ", ";
		} 
		//Zadnji element
		if (nizNevalidnih.length>0) {
			poruka += nizNevalidnih[duzinaNiza-1] + "!";
		}
		else poruka="";
		console.log(poruka);
		divElementPoruke.innerHTML= poruka;
	}
	
	var konstruktor=function(divElementPoruke){
		nizNevalidnih = []; poruka = "";
		return{
			ime:function(inputElement){
				var valid = true;
				if (inputElement==null) valid = false;
				else {
					var imeString = inputElement.value;
					var imeRegex = /^([A-Z]((\'){0,1}[a-z])+)([\s-][A-Z]((\'){0,1}[a-z])+){0,3}$/;
					var valid = imeRegex.test(imeString);
					/*Validno ime predstavlja maksimalno četri riječi koje počinju velikim slovom, svaka riječ ne
					smije biti kraća od dva slova. Riječi mogu biti odvojene razmakom ili crticom. Unutar riječi
					se može pojaviti apostrof ali nikada odmah do drugog apostrofa*/
					
				}
				if (!valid) {
					//Pozadina elementa orangered
					nizNevalidnih.push("ime");
					inputElement.style.backgroundColor="orangered";
				}
				else {
					inputElement.style.backgroundColor="white";
				}
				ispisi(divElementPoruke);
				return valid;
			},
			godina:function(inputElement){
				if (inputElement == null) {
					return;
				}
				var valid = true;
				var godinaString = inputElement.value;
				console.log(godinaString);
				var godinaRegex = /^(20)[0-9]{2}\/(20)[0-9]{2}$/;
				var valid = godinaRegex.test(godinaString);
				/*Validna godina predstavlja string oblika 20AB/20CD gdje je CD broj za jedan veći od AB.
				*/
				//Provjera da li su godine susjedne
				var g1 = parseInt(godinaString[2]+godinaString[3]);
				var g2 = parseInt(godinaString[7]+godinaString[8]);
				if (!valid || g1+1!=g2) {
					//Pozadina elementa orangered
					nizNevalidnih.push("godina");
					inputElement.style.backgroundColor="orangered";
				}
				else {
					inputElement.style.backgroundColor="white";
				}
				ispisi(divElementPoruke);
				return valid;
			},
			repozitorij:function(inputElement,regex){
				var valid = true;
				var repoString = inputElement.value;
				console.log(regex);
				var valid = regex.test(repoString);
				console.log(valid);
				//Validan repozitorij predstavlja string koji zadovoljava regex iz parametra
				if (!valid) {
					nizNevalidnih.push("repozitorij");
					inputElement.style.backgroundColor="orangered";
				}
				else {
					inputElement.style.backgroundColor="white";
				}
				ispisi(divElementPoruke);
				return valid;
			},
			index:function(inputElement){
				console.log(inputElement.value);
				var valid = true;
				var indexString = inputElement.value;
				/*var regex = /^\d\d\d\d\d$/;
				valid = regex.test(indexString);
				if (!valid)
					return false;*/

				brIndexa = parseInt(indexString);
				if (inputElement.value==null || inputElement.value=="" || isNaN(brIndexa)) {
					nizNevalidnih.push("index");
					inputElement.style.backgroundColor="orangered";
					ispisi(divElementPoruke);
					return;
				}
				if (brIndexa < 14000 || brIndexa>=21000) valid = false;
				//Validan index je petocifren pozitivan broj gdje su prve dvije cifre od 14 do 20.
				if (!valid) {
					nizNevalidnih.push("index");
					inputElement.style.backgroundColor="orangered";
				}
				else {
					inputElement.style.backgroundColor="white";
				}
				console.log(nizNevalidnih);
				ispisi(divElementPoruke);
				return valid;
			},
			naziv:function(inputElement){
				var valid = true;
				var nazivString = inputElement.value;
				var nazivRegex = /^[a-zA-Z][a-zA-Z0-9\\\/\-\"\'\!\?\:\;\,]+(\d|[a-z])$/
				valid = nazivRegex.test(nazivString);				
				if (!valid) {
					nizNevalidnih.push("naziv");
					inputElement.style.backgroundColor="orangered";
				}
				else {
					inputElement.style.backgroundColor="white";

				}
				ispisi(divElementPoruke);
				return valid;
			},
			password:function(inputElement){
				var valid = true;
				var passwordString = inputElement.value;
				//Dodatni uslov
				var passwordRegex = /^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})/
				valid = passwordRegex.test(passwordString);
				if (!valid) {
					nizNevalidnih.push("password");
					inputElement.style.backgroundColor="orangered";
				}
				else {
					inputElement.style.backgroundColor="white";
				}
				ispisi(divElementPoruke);
				return valid;
			},
			url:function(inputElement){
				var valid = true;
				if (inputElement==null) valid = false;
				else {
					var urlString = inputElement.value;
					//Validacija URL-a
					//Stari regex, rijec bez crtice: ^(http|https|ftp|ssh)\:\/\/\w+(\.\w+)*(((\/\w+)+)(\?[a-z0-9]([a-z0-9-]*[a-z0-9])*\=[a-z0-9]([a-z0-9-]*[a-z0-9])*)*){0,1}$
					var urlRegex = /^(http|https|ftp|ssh)\:\/\/[a-z0-9]([a-z0-9-]*[a-z0-9])*(\.[a-z0-9]([a-z0-9-]*[a-z0-9])*)*(((\/([a-z0-9]([a-z0-9-]*[a-z0-9])*)+)+)(\?[a-z0-9]([a-z0-9-]*[a-z0-9])*\=[a-z0-9]([a-z0-9-]*[a-z0-9])*)*){0,1}(\&[a-z0-9]([a-z0-9-]*[a-z0-9])*\=[a-z0-9]([a-z0-9-]*[a-z0-9])*)*$/
					valid = urlRegex.test(urlString);
				}
				if (!valid) {
					nizNevalidnih.push("url");
					inputElement.style.backgroundColor="orangered";
				}
				else {
					inputElement.style.backgroundColor="white";
				}
				ispisi(divElementPoruke);
				return valid;
			}

		}
	}
	return konstruktor;
}());

