//Test modula za zadatak 8
/*Swindow.onload = function() {
    var zadaciAjax = new ZadaciAjax(function(nesto) {
        console.log(nesto);
    });
    zadaciAjax.dajCSV();
    zadaciAjax.dajJSON();
    zadaciAjax.dajXML();
}*/

//------------------------------------------------------
//Zadatak 8.
//------------------------------------------------------
var ZadaciAjax = (function(){
    let broj_pozvanih=0;
    var konstruktor = function(callbackFn){
        broj_pozvanih=0;
        return {
            //Prave GET zahtjeve sa XMLHttpRequest na http://localhost:8080/zadaci
            dajXML:function(){
                broj_pozvanih++;
                if (broj_pozvanih>1) callbackFn("{\"greska\":\"Već ste uputili zahtjev\"}");
                //Pravimo zahtjev
                console.log("dajXML");
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    //Ispis godina
                    if (this.readyState == 4 && this.status == 200) {
                        //Poziv funkcije callbackFn
                        console.log(xhttp.response);
                        callbackFn(xhttp.response);
                        broj_pozvanih--;
                    }
                }
                xhttp.open("GET", "http://localhost:8080/zadaci", true);
                xhttp.setRequestHeader('Accept', 'application/xml');
                xhttp.timeout = 2000;
                xhttp.ontimeout = function (e) {
                    // XMLHttpRequest timed out. Do something here.
                    xhttp.abort();
                };
                xhttp.send();
            },
            dajCSV:function(){
                broj_pozvanih++;
                if (broj_pozvanih>1) callbackFn("{greska:\"Već ste uputili zahtjev\"}");
                //Pravimo zahtjev
                console.log("dajCSV");
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    //Ispis godina
                    if (this.readyState == 4 && this.status == 200) {
                        //Poziv funkcije callbackFn
                        callbackFn(xhttp.response);
                        broj_pozvanih--;
                    }
                }
                xhttp.open("GET", "http://localhost:8080/zadaci", true);
                xhttp.setRequestHeader('Accept', 'text/csv');
                xhttp.timeout = 2000;
                xhttp.ontimeout = function (e) {
                    // XMLHttpRequest timed out. Do something here.
                    xhttp.abort();
                };
                xhttp.send();
            },
            dajJSON:function(){
                broj_pozvanih++;
                if (broj_pozvanih>1) callbackFn("{greska:\"Već ste uputili zahtjev\"}");
                //Pravimo zahtjev
                console.log("dajJSON");
               
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    //Ispis godina
                    if (this.readyState == 4 && this.status == 200) {
                        //Poziv funkcije callbackFn
                        console.log(xhttp.response);
                        callbackFn(xhttp.response);
                        broj_pozvanih--;
                    }
                }
                xhttp.open("GET", "http://localhost:8080/zadaci", true);
                xhttp.setRequestHeader('Accept', 'application/json');
                xhttp.timeout = 2000;
                xhttp.ontimeout = function (e) {
                    // XMLHttpRequest timed out. Do something here.
                    xhttp.abort();
                };
                xhttp.send();
            }
        }
    }
    return konstruktor;
}());

    