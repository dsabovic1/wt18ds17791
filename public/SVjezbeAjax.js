var SVjezbeAjax = (function() {
    var konstruktor = function(sVjezbe) {
        //Pravimo zahtjev 
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            //Upis naziva godina u listu
            json_niz = JSON.parse(this.response);
            console.log("SVjezbe:"+ json_niz);
            if (sVjezbe.options.length>0) { //Brisemo postojece u listi
                var i;
                for(i = sVjezbe.length - 1 ; i >= 0 ; i--)
                {
                    sVjezbe.options[i] = null;
                }
            }
            if (!Array.isArray(json_niz)) { //Response nije uvijek JSON niz
                if (json_niz) {
                    var option = document.createElement("option");
                    option.value = json_niz.id;
                    option.text = json_niz.nazivVjezbe;
                    sVjezbe.add(option);
                }  
            }
            else {
                for (i in json_niz) {  
                    nazivVjezbe = json_niz[i].nazivVjezbe;
                    var option = document.createElement("option");
                    option.value = json_niz[i].id;
                    option.text = nazivVjezbe;
                    sVjezbe.add(option);
                }
            }
            if (sVjezbe.id == "sVjezbef3") {
                var zadaci = document.getElementById("sZadatakf3");
	            new SZadaciAjax(zadaci);
            }

        }
        xhttp.open("GET", "http://localhost:8080/vjezbeBaza", true);
        xhttp.setRequestHeader('Content-Type', 'application/json');
        xhttp.send();
    }
    return konstruktor;
}());