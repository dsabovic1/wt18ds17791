//Primjer koristenja:
//console.log("Broj zadatka: "+rbZadatka+"\nurl: " + url);
var mojDiv=document.getElementById("commiti");
var tabela = new CommitTabela(mojDiv,8);

function addCommit() {
	if (validirajAddCommit()) { //Validan comit
		var rbZadatka=parseInt(document.getElementById("rbZadatkaAdd").value);
		var url = document.getElementById("urlAdd").value;
		//console.log("Parametri: " + rbZadatka + " " + url);
		tabela.dodajCommit(rbZadatka-1,url); //Korisnik indeksira od 1, a u modulu se koristi indeksacija od 0
		return;
	}
	console.log("Nevlidni parametri");
	/*U zadatku ne smijete koristiti nikakve hardkodirane vanjske
	elemente, niti bilo kakvu vanjsku variablu koju niste dobili kroz
	parametre konstruktora ili kroz parametre metoda*/
}

function editCommit() {
	if (validirajEditCommit()) {
		var rbZadatka=parseInt(document.getElementById("rbZadatkaEdit").value);
		var rbCommita=parseInt(document.getElementById("rbCommitaEdit").value);
		var url = document.getElementById("urlEdit").value;
		//console.log(rbZadatka + " " + rbCommita + " " + url);
		tabela.editujCommit(rbZadatka-1, rbCommita-1, url);
		return;
	}
	console.log("Nevlidni parametri");
}

function deleteCommit() {
	if (validirajDeleteCommit()) {
		var rbZadatka=parseInt(document.getElementById("rbZadatkaDelete").value);
		var rbCommita=parseInt(document.getElementById("rbCommitaDelete").value);
		tabela.obrisiCommit(rbZadatka-1, rbCommita-1); 
		return;
	}
	console.log("Nevlidni parametri");
}

//Validacija addCommit
function validirajAddCommit() {
	var mojDivAdd=document.getElementById("porukaAddCommit");
	var inputUrl=document.getElementById("urlAdd");
	var validacija = new Validacija(mojDivAdd);
	return validacija.url(inputUrl);
}


//Validacija editCommit
function validirajEditCommit() {
	var mojDivEdit=document.getElementById("porukaEditCommit");
	var inputUrl=document.getElementById("urlEdit");
	var validacija = new Validacija(mojDivEdit);
	return validacija.url(inputUrl);
}


//Validacija deleteCommit
function validirajDeleteCommit() {
	var rbZ=parseInt(document.getElementById("rbZadatkaDelete"));
	var rbC=parseInt(document.getElementById("rbCommitaDelete"));
	if (rbZ<=0 || rbC<=0) return false;
	return true;
}
